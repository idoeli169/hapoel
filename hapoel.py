def main():
	""" this is the "main" function, it puts the data in a variable(also the players data) and calls the function
		that creates the pages in a loop (each loop it creats a page (for other player of course)) """
	data = open(r"template.htm","r")
	html_list = data.read() # gets the html file
	html_list = html_list.split("%%")
	data.close()
	
	team = open(r"players.csv","r")
	players = team.readlines() # gets the the "data" of the players (info)
	team.close()
	
	keys = players[0].split(",") # splits the values
	del players[0] # delets the first line (" birth,name,number,position,id ")
	
	for line in players:
		values = line.split(",") # splits the birth, name etc. 
		file_creator(keys, values, html_list) # calls the function that will create the pages

def file_creator(keys, values, html_list): 
	""" This func creates the pages of the players and put the right info between the "%% %%" """
	player = {} # created a dictionary for the player and later on will have his info
	place = 0
	
	for num in range(len(keys)):
		player[keys[num]] = values[num]
		
	player_page = list(html_list)
	
	# puts the right info between the "%% %%"
	for word in player_page:
		if( word in player.keys() ):
			player_page[place] = player[word]
			
		elif( word+"\n" in player.keys() ):
			player_page[place] = player[word+"\n"][:len(player[word+"\n"])-1]
		place = place + 1
		
	player_page = "".join(player_page)
	# --------------------------------------- #
	
	file_name = player["id\n"][:4]+".htm" # creates a name for the file (the name is the player's id)
	file_name = r"htmfiles\\" + (file_name) # tells the file where it needs to go
	
	player_file = open(file_name,"w")
	player_file.write(player_page)# creates the file
	player_file.close()

if __name__ == "__main__":
        main()
